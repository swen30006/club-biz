@c = Club.find_by([

	{id: 1,name:  "CISSA",description: "This is computer system club",contact: "Brian : 012345678"}, 
	{id: 2,name: "IVSUM",description: "This is Vietnamese club",contact: "Tom : 0426150070"},
	{id: 3,name: "IIPA",description: "This is Indonesian club",contact: "Erlangga : 043535343"}

	])

@e = Event.find_by ([
	{id:1,name: "Pizza lunch",location: "South Lawn",description: "This is Pizza lunch for IT geeks",
		date: Date.new(2014, 2, 4), time:Time.new(2014, 6, 24, 7, 2, 6),:org_club_id => 1},
	{id:2,name: "Tuesday lunch",location: "Law building",description: "This is Tuesday lunch for vietnamese",
		date: Date.new(2014, 11, 10), time:Time.new(2012, 8, 29, 12, 34, 56) ,:org_club_id => 2}
	
	])

@t = Ticket.find_by ([
	{id:1, name: "cheap", price: 5, msg: "Only allow eat pizza",max_no: 5,
		event_id:1, start_date: Date.new(2014, 11, 10),close_date:Date.new(2014, 11, 10)},
	{id:2, name: "normal", price: 10, msg: "allow eat bbq and pizza",max_no: 20,
		event_id:1, start_date: Date.new(2014, 11, 10),close_date:Date.new(2014, 11, 10)}
	])

@b = Booking.find_by ([
	{id:1, ticket_id: 1, student_id: 1, num_tickets: 4},
	
	])