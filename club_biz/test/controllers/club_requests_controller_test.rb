require 'test_helper'

class ClubRequestsControllerTest < ActionController::TestCase
  setup do
    @club_request = club_requests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:club_requests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create club_request" do
    assert_difference('ClubRequest.count') do
      post :create, club_request: { admin: @club_request.admin, contact: @club_request.contact, name: @club_request.name, regis_num: @club_request.regis_num }
    end

    assert_redirected_to club_request_path(assigns(:club_request))
  end

  test "should show club_request" do
    get :show, id: @club_request
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @club_request
    assert_response :success
  end

  test "should update club_request" do
    patch :update, id: @club_request, club_request: { admin: @club_request.admin, contact: @club_request.contact, name: @club_request.name, regis_num: @club_request.regis_num }
    assert_redirected_to club_request_path(assigns(:club_request))
  end

  test "should destroy club_request" do
    assert_difference('ClubRequest.count', -1) do
      delete :destroy, id: @club_request
    end

    assert_redirected_to club_requests_path
  end
end
