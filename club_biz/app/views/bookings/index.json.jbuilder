json.array!(@bookings) do |booking|
  json.extract! booking, :id, :num_tickets
  json.url booking_url(booking, format: :json)
end
