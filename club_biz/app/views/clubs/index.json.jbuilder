json.array!(@clubs) do |club|
  json.extract! club, :id, :name, :description, :contact
  json.url club_url(club, format: :json)
end
