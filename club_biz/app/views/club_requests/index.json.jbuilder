json.array!(@club_requests) do |club_request|
  json.extract! club_request, :id, :name, :regis_num, :admin, :contact
  json.url club_request_url(club_request, format: :json)
end
