json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :name, :price, :start_date, :close_date, :msg, :max_no
  json.url ticket_url(ticket, format: :json)
end
