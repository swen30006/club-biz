# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class TicketsController < ApplicationController
before_action :set_ticket, only: [:show, :edit, :update, :destroy]

	# GET /tickets
	# GET /tickets.json
	# method to displays page contains all the tickets of a event information
	def index
		@tickets = Ticket.all
	end

	# GET /tickets/1
	# GET /tickets/1.json
	# method to show a type of ticket of a event
	def show
	end

	# GET /tickets/new
	# method to make a type of ticket of a event and display the form 
	def new
		@ticket = Ticket.new(session[:ticket_params])
	end

	# GET /tickets/1/edit
	# method to edit a specify ticket
	def edit
	end

	# POST /tickets
	# POST /tickets.json
	# method to create a type of ticket of a event and save it in the database
	def create
		session[:ticket_params] = ticket_params
		@ticket = Ticket.new(session[:ticket_params])
		@event = Event.find_by(id: session[:event_id])

		respond_to do |format|

			if !@ticket.save
				format.html { render action: 'new' }
				format.json { render json: @ticket.errors, status: :unprocessable_entity }
			end

			@event.tickets << @ticket

			session[:event_id] = nil
			session[:ticket_params] = nil

			format.html { redirect_to @event, notice: 'Ticket was successfully added.' }
		end
	end

	# PATCH/PUT /tickets/1
	# PATCH/PUT /tickets/1.json
	# method to update a type of ticket of a event
	def update
		respond_to do |format|
			if @ticket.update(ticket_params)
				format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
				format.json { head :no_content }
			else
				format.html { render action: 'edit' }
				format.json { render json: @ticket.errors, status: :unprocessable_entity }
			end
		end
	end

	# DELETE /tickets/1
	# DELETE /tickets/1.json
	# method to cancel a type of ticket of a event
	def destroy
		@ticket.destroy
		respond_to do |format|
			format.html { redirect_to tickets_url }
			format.json { head :no_content }
		end
	end

	private
		# Use callbacks to share common setup or constraints between actions.
		def set_ticket
			@ticket = Ticket.find(params[:id])
		end

		# Never trust parameters from the scary internet, only allow the white list through.
		def ticket_params
			params.require(:ticket).permit(:name, :price, :start_date, :close_date, :msg, :max_no)
		end
end
