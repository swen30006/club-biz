# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy, :comment]

  # GET /events
  # GET /events.json
  # method to displays all the events
  def index
    @events = Event.all
  end

  # GET /events/1
  # GET /events/1.json
  # method to show a specify event
  def show
    @total_tickets=0
    @total_revenue=0
    @ticket = Ticket.where(event_id: params[:id])
    @booking_records= Array.new

    @booking_records = @event.bookings
    @total_tickets = @event.num_booked_tickets
    @total_revenue = @event.revenue

    @comments = Comment.where(event_id: @event.id)

    @comments = @comments.sort_by{:created_at}.reverse
  end

  # GET /events/new
  # method to initialize a form to create a new event
  def new
    @event = Event.new(session[:event_params])
  end

  # GET /events/1/edit
  # method to edit a specify event
  def edit
  end

  # POST /events
  # POST /events.json
  # method to create a event and save it in the database
  def create
    @event = Event.new(event_params)
    @club = Club.find_by(id: session[:club_id])

    respond_to do |format|
      if !@event.save
        format.html { render action: 'new' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    
      @club.org_events << @event

      session[:club_id] = nil

      format.html { redirect_to @event, notice: 'Event was successfully created.' }
      format.json { render action: 'show', status: :created, location: @event }
    end

  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  # method to update a event
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  # method to cancel a bookings
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url }
      format.json { head :no_content }
    end
  end

  # POST 'events/add_ticket'
  # method to initialize to add a new type of ticket to a event
  def add_ticket
    session[:event_id] = params[:event_id]
    redirect_to new_ticket_path
  end

  # POST 'events/book_ticket'
  # method to initialize to book a type of ticket to a event
  def book_ticket
    session[:event_id] = params[:event_id]
    redirect_to new_booking_path
  end

  # POST 'events/:id/invite
  # method to initialize a invite form to invite other clubs to a event
  def invite
    @all_clubs = Club.all
    @event = Event.find_by(id: params[:event_id])
  end

  # POST 'events/:id/submit_invite'
  # method to send invite to invite other clubs to a event
  def submit_invite
    @event = Event.find_by(id: params[:id])
    @club_ids = params[:club_ids]
    
    respond_to do |format|
      @club_ids.each do |club_id|
        @invite_club = Club.find_by(id: club_id)
        @event.invited_clubs << @invite_club
      end

      format.html { redirect_to @event, notice: 'Invitation successful' }
      format.json { render action: 'show', status: :created, location: @event }
    end
  end

  # POST 'events/:id/comment'
  # method to comment on a event
  def comment
    if !student_signed_in?
        redirect_to event_path, notice: 'You must log in to comment'
    else
    @comment = Comment.new(content: params[:comment][:content])
    respond_to do |format|
      if @comment.save
        format.html { redirect_to @event, notice: 'Your comment was successfully created.' }
        format.json { render action: 'show', status: :created, location: @comment }

        current_student.comments << @comment
        @event.comments << @comment
      else
        format.html { render action: 'new' }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:name, :date, :time, :description, :location)
    end
end
