# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class ClubRequestsController < ApplicationController
  before_action :set_club_request, only: [:show, :edit, :update, :destroy]

  # GET /club_requests
  # GET /club_requests.json
  # method to displays all requests for club registration
  def index
    @club_requests = ClubRequest.all
  end

  # GET /club_requests/1
  # GET /club_requests/1.json
  # method to show a specify club request for registration
  def show
  end

  # GET /club_requests/new
  # method to initialize a form to create a new club registration
  def new
    @club_request = ClubRequest.new
  end

  # GET /club_requests/1/edit
  # method to edit a specify club registration
  def edit
  end

  # POST /club_requests
  # POST /club_requests.json
  # method to create a club requests and save it in the database to wait for uni admin to accept
  def create
    @club_request = ClubRequest.new(club_request_params)

    respond_to do |format|
      if @club_request.save
        format.html { redirect_to @club_request, notice: 'Club request was successfully created.' }
        format.json { render action: 'show', status: :created, location: @club_request }
      else
        format.html { render action: 'new' }
        format.json { render json: @club_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /club_requests/1
  # PATCH/PUT /club_requests/1.json
  # method to update a club request for registration
  def update
    respond_to do |format|
      if @club_request.update(club_request_params)
        format.html { redirect_to @club_request, notice: 'Club request was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @club_request.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /club_requests/1
  # DELETE /club_requests/1.json
  # method to cancel a club registration
  def destroy
    @club_request.destroy
    respond_to do |format|
      format.html { redirect_to club_requests_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_club_request
      @club_request = ClubRequest.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def club_request_params
      params.require(:club_request).permit(:name, :regis_num, :admin, :contact)
    end
end
