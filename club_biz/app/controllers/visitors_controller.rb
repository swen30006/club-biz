# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class VisitorsController < ApplicationController

    # root "visitors#index"
    # method to return a array of members belong to a club which organize a event
    def index
    	if admin_signed_in?
    		redirect_to admins_index_path
    	end
    	if student_signed_in?
    		redirect_to students_index_path
    	end
    	@clubs = Club.all
    	@events = Event.all
    	
    end
end
