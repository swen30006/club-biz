# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014



class AdminsController < ApplicationController

	# GET 'admins/index'
	# method to displays all the club requests for registration
	def index
		@clubs = ClubRequest.all
	end

	# POST 'admins/accept'
	# method to accept a club request for registration by uni admin
	def accept
	  	@request = ClubRequest.find_by(id: params[:request_id])
	  	@club = Club.new()
	  	@admin = @request.admin

	  	if(@admin!=nil)
		  	@club.name = @request.name
		  	@club.reg_num = @request.regis_num
		  	@club.contact = @request.contact
		  	@club.admin = @admin

		  	@club.save

		  	@request.destroy

		  	# email the congrations to the club admin got his club accepted
		  	AdminMailer.club_request_accept(@admin,@request).deliver
		  	redirect_to admins_index_path, notice: 'Club was successfully created.'
  		else
  			# email the congrations to the club admin to ask him to resolve the problem by edit his request
  			AdminMailer.club_request_error(@admin,@request).deliver
  			redirect_to admins_index_path, notice: 'Club was not successfully created: The admin email address is not registered with the university'
  		end

 	end

 	# POST 'admins/reject'
 	# method to accept a club request for registration by uni admin
	def reject
		@request = ClubRequest.find_by(id: params[:request_id])
		@admin = @request.admin
		AdminMailer.club_request_reject(@admin,@request).deliver
		@request.destroy
	  	redirect_to admins_index_path
	end

end
