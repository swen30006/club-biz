# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class SearchController < ApplicationController
	# GET "search/index"
	# method to displays the search results for both clubs and events
	def index
	  	@clubs = Club.search(params[:search])
	  	@events = Event.search(params[:search])
	end
end
