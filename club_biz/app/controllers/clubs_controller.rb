# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class ClubsController < ApplicationController
  before_action :set_club, only: [:show, :edit, :update, :destroy, :accept_invite, :reject_invite]

  # GET /clubs
  # GET /clubs.json
  # method to displays all the clubs
  def index
    @clubs = Club.all
  end

  # GET /clubs/1
  # GET /clubs/1.json
  # method to show a specify club
  def show
    @club = Club.find(params[:id])
    @participating_events = @club.participating_events
    @announcements = @club.announcements
  end

  # GET /clubs/new
  # method to initialize a form to create a new club 
  def new
    @club = Club.new
  end

  # GET /clubs/1/edit
  # method to edit a specify club
  def edit
  end

  # POST /clubs
  # POST /clubs.json
  # method to create a club and save it in the database
  def create
    @club = Club.new(club_params)

    respond_to do |format|
      if @club.save
        format.html { redirect_to @club, notice: 'Club was successfully created.' }
        format.json { render action: 'show', status: :created, location: @club }
      else
        format.html { render action: 'new' }
        format.json { render json: @club.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /clubs/1
  # PATCH/PUT /clubs/1.json
  # method to update a club
  def update
    respond_to do |format|
      if @club.update(club_params)
        format.html { redirect_to @club, notice: 'Club was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @club.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /clubs/1
  # DELETE /clubs/1.json
  # method to cancel a club
  def destroy
    @club.destroy
    respond_to do |format|
      format.html { redirect_to clubs_url }
      format.json { head :no_content }
    end
  end

  # method to subcribe a club by a student
  def subscribe
    @club = Club.find(params[:club_id])
    begin
      current_student.subscribed_clubs.find(params[:club_id])
    rescue Exception => e
      if student_signed_in?
        if current_student.subscribed_clubs << @club
          redirect_to @club, notice: 'You have successfully subscribed to this club.'
        else
          redirect_to @club, notice: 'Subscription was unsuccessful. Please try again later.'
        end
      else
          redirect_to @club, notice: 'You have to sign in to subscribe'
      end

    else
      redirect_to @club, notice: 'You are already subscribed to this club.'
    end
  end

  # method to unsubcribe a club by a student
  def unsubscribe
    begin
      @club = current_student.subscribed_clubs.find(params[:club_id])    
    rescue Exception => e
      @tmp = Club.find(params[:club_id])
      redirect_to @tmp, notice: 'You are not subscribed to this club.'
    else
      @tmp = @club
      if current_student.subscribed_clubs.delete(@club)
        redirect_to @tmp, notice: 'You have successfully unsubscribed to this club.'
      else
        redirect_to @tmp, notice: 'Unsubscription was unsuccessful. Please try again later.'
      end
    end
  end

  # method to initialize a form to create a event
  def create_event
    session[:club_id] = params[:club_id]
    redirect_to new_event_path
  end

  # method to post announcement to clubs members by club admin
  def announce
    @announcement = Announcement.new(content: params[:announcement][:content])
    @club = Club.find_by(id: params[:id])

    respond_to do |format|
      if @announcement.content!='' && @announcement.save
        format.html { redirect_to @club, notice: 'Announcement was successfully created.' }
        format.json { render action: 'show', status: :created, location: @announcement }
        @club.announcements << @announcement
      else
        format.html { redirect_to @club, notice: 'Announcement was not created.' }
        format.json { render json: @announcement.errors, status: :unprocessable_entity }
      end
    end

  end

  # method to accept invitation for a event by club admin
  def accept_invite
    @event = Event.find_by(id: params[:event_id])
    @club.accepted_events << @event
    @club.invited_events.delete(@event)
    @event.invited_clubs.delete(@club)

    redirect_to @club, notice: 'Invitation successfully accepted.'
  end

  # method to reject invitation for a event by club admin
  def reject_invite
    @event = Event.find_by(id: params[:event_id])
    @club.rejected_events << @event
    @club.invited_events.delete(@event)
    @event.invited_clubs.delete(@club)

    redirect_to @club, notice: 'Invitation successfully rejected.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_club
      begin
        @club = Club.find(params[:id])
      rescue Exception => e
        render file: 'public/404', status: 404, formats: [:html]
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def club_params
      params.require(:club).permit(:name, :description, :contact)
    end
end
