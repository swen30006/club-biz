# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class StudentsController < ApplicationController

	# GET "students/index"
	# method to displays Student homepage
  	def index	
	    @clubs_member = current_student.member_clubs
	    @clubs_subscriber = current_student.subscribed_clubs
	    @clubs_admin = current_student.admin_club

	    @events= Event.all 
	    @tickets= current_student.tickets

	    @my_events = Array.new

	    @tickets.each do |ticket| 
			if !@my_events.include?(ticket.event)
				@my_events << ticket.event
			end
		end										

	    @clubs_subscriber_id = @clubs_subscriber.pluck(:id)
	    @announcements = current_student.announcements

	    @announcements = @announcements.sort_by{:created_at}.reverse
		    
	end
end
