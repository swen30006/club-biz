# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class BookingsController < ApplicationController
  before_action :set_booking, only: [:show, :edit, :update, :destroy]

  # GET /bookings
  # GET /bookings.json
  # method to displays all the bookings
  def index
    @bookings = Booking.where(student_id: current_student.id)
  end

  # GET /bookings/1
  # GET /bookings/1.json
  # method to show a specify booking
  def show
  end

  # GET /bookings/new
  # method to make a new booking session and display the form 
  def new
    @event=Event.find_by(id: session[:event_id])

    if @event.members.include?(current_student)

      if Booking.total_num_tickets(current_student.id,session[:event_id]) < 4
        @tickets = Event.find_by(id: session[:event_id]).tickets
        @booking = Booking.new(session[:booking_params])
      else
        redirect_to @event, notice: 'You already booked max number of tickets!'
      end
  
    else
      redirect_to @event, notice: 'You must be a member to book tickets of this club!'
    end
  end

  # GET /bookings/1/edit
  # method to edit a specify booking
  def edit
    
    @tickets =@booking.ticket.event.tickets
  end

  # POST /bookings
  # POST /bookings.json
  # method to confirm a booking and save it in the database
  def create

    @booking = Booking.new(booking_params)
    @ticket = Ticket.find_by(id: params[:ticket_id])
    @student= current_student
    @booking.student = current_student
    @booking.ticket = @ticket
    @event = @ticket.event

    if (Booking.total_num_tickets(current_student.id,session[:event_id])+ @booking.num_tickets) > 4
      redirect_to @event, notice: 'You tried to book more than 4 tickets ! Try to book smaller number !'

    else 

      respond_to do |format|
      if @booking.save
        BookingMailer.booking_confirmation(@student,@booking,@event).deliver
        format.html { redirect_to @ticket.event, notice: 'Booking was successfully created.' }
        format.json { render action: 'show', status: :created, location: @booking }
      else
        format.html { render action: 'new' }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end

      session[:event_id] = nil
      session[:booking_params] = nil

      end

    end

  end

  # PATCH/PUT /bookings/1
  # PATCH/PUT /bookings/1.json
  # method to update a booking
  def update
    respond_to do |format|
      if @booking.update(booking_params)
        format.html { redirect_to @booking, notice: 'Booking was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @booking.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bookings/1
  # DELETE /bookings/1.json
  # method to cancel a bookings
  def destroy
    @booking.destroy
    respond_to do |format|
      format.html { redirect_to bookings_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_booking
      @booking = Booking.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def booking_params
      params.require(:booking).permit(:num_tickets)
    end
end
