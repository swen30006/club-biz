class AdminMailer < ActionMailer::Base
  default :from => "club.biz.2014@gmail.com"

	def club_request_accept(admin,request)
		@admin=admin
		@request=request
    	mail(:to => admin.email, :subject => "Club Request Response for Club-biz")
  	end

  	def club_request_reject(admin,request)
		@admin=admin
		@request=request
    	mail(:to => admin.email, :subject => "Club Request Response for Club-biz")
  	end

  	def club_request_error(admin,request)
		@admin=admin
		@request=request
    	mail(:to => request.admin, :subject => "Club Request Response for Club-biz")
  	end

end
