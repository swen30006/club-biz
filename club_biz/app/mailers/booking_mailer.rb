class BookingMailer < ActionMailer::Base
	default :from => "club.biz.2014@gmail.com"

	def booking_confirmation(student,booking,event)
		@student=student
		@booking=booking
		@event=event
    	mail(:to => student.email, :subject => "Booking Confirmation")
  end
end
