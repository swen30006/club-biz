# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class Student < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :bookings
  has_many :tickets, through: :bookings
  has_and_belongs_to_many :member_clubs, class_name: "Club",join_table: "clubs_members"
  has_and_belongs_to_many :subscribed_clubs, class_name: "Club",join_table: "clubs_subscribers"
  has_many :admin_club, class_name: "Club", foreign_key: "admin_id"
  has_many :comments

  # display the full name for a student
  def full_name
    return first_name + " " + last_name
  end

  def announcements
    return Announcement.find_all_by_club_id(@clubs_subscriber_id)
  end
end
