# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class Club < ActiveRecord::Base
	has_and_belongs_to_many :invited_events, class_name: "Event",join_table: "invited_clubs_events"
	has_and_belongs_to_many :accepted_events, class_name: "Event",join_table: "accepted_clubs_events"
	has_and_belongs_to_many :rejected_events, class_name: "Event",join_table: "rejected_clubs_events"
	has_many :org_events, class_name: "Event", foreign_key: "org_club_id"
	has_and_belongs_to_many :members, class_name: "Student", join_table: "clubs_members"
	has_and_belongs_to_many :subscribers, class_name: "Student", join_table: "clubs_subscribers"
	belongs_to :admin, class_name: "Student"
	has_many :announcements
	
	# accepted events + organising events
	def participating_events
		events = Array.new
		events << self.accepted_events
		events << self.org_events
		return events.flatten
	end

	# method to search the record based on a search term
	def self.search(search)
  		if search
    		find(:all, :conditions => ['name LIKE ? or description LIKE ? or contact LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%"])
  		else
    		find(:all)
  		end
	end
end
