# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class Event < ActiveRecord::Base
	has_many :tickets
	has_and_belongs_to_many :accepted_clubs, class_name: "Club",join_table: "accepted_clubs_events"
	has_and_belongs_to_many :rejected_clubs, class_name: "Club",join_table: "rejected_clubs_events"
	has_and_belongs_to_many :invited_clubs, class_name: "Club",join_table: "invited_clubs_events"
	belongs_to :org_club, class_name: "Club"
	has_many :comments

	# method to return a array of admins belong to a club which organize a event
	def admins
		admins_array = Array.new
		admins_array << self.org_club.admin
		self.accepted_clubs.each do |club|
			admins_array << club.admin
		end
		return admins_array.flatten
	end

	# method to return a array of members belong to a club which organize a event
	def members
		members_array = Array.new
		members_array << self.org_club.members
		self.accepted_clubs.each do |club|
			members_array << club.members
		end
		return members_array.flatten
	end

	def bookings
		@bookings = Array.new
		self.tickets.each do |ticket|
      		if Booking.where(ticket_id: ticket.id).any?
        		@bookings << Booking.where(ticket_id: ticket.id)
      		end
    	end
    	return @bookings
    end

    def num_booked_tickets
    	@booked_tickets = 0

    	if !self.bookings.empty?
	    	self.bookings.each do |relation|
	    		relation.each do |booking|
	    			@booked_tickets += booking.num_tickets
	    		end
	    	end
	    end
	    return @booked_tickets
	end

	def revenue
    	@revenue = 0

    	if !self.bookings.empty?
	    	self.bookings.each do |relation|
	    		relation.each do |booking|
	    			@revenue += booking.ticket.price * booking.num_tickets
	    		end
	    	end
	    end
	    return @revenue
	end

	def can_invite_clubs?
		return  if @event.org_club != club && !@event.invited_clubs.include?(club) && 
		!@event.accepted_clubs.include?(club) && !@event.rejected_clubs.include?(club)
	end

	# method to search the record based on a search term
	def self.search(search)
  		if search
    		find(:all, :conditions => ['name LIKE ? or description LIKE ? or location LIKE ? or date LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%"])
  		else
    		find(:all)
  		end
	end

end
