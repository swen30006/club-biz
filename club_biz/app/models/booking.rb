# SWEN30006  Software Modelling and Design
# THE CLUB-BIZ WEB-APP
# Author: Jason Lee - 591740
#         Seunghwa Kang - 602027
#         Huynh Nguyen Minh Thong - 598093
# Start date : March 22 2014
# Finish date: May 25 2014

class Booking < ActiveRecord::Base
	belongs_to :student
	belongs_to :ticket

	# method to find a total number of tickets booked by a student for a particle event
	def self.total_num_tickets(s_id,e_id)
		@total_bookings = Array.new
		@tickets = Ticket.where(event_id: e_id)
		@no_of_tickets=0

		@tickets.each do |ticket|
			@total_bookings = Booking.where(student_id: s_id, ticket_id: ticket.id)
			@total_bookings.each do |booking|
				@no_of_tickets+=booking.num_tickets
			end
		end

		return @no_of_tickets
	end

end
