# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
ClubBiz::Application.config.secret_key_base = 'd45d62d8d5195eee24aa81f09df2cec5f9d0d9507af5b8866bd48d7f101bd2f6357d18c9cdf710a16c1ad8273792e6f0770c5e26b2996919cdfae3e1dddd96b9'
