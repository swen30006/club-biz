ClubBiz::Application.routes.draw do  
  get "search/index"
  resources :club_requests

  devise_for :admins
  get 'admins/index' => 'admins#index', as: :admins_index
  post 'admins/accept' => 'admins#accept', as: :accept_club_request
  post 'admins/reject' => 'admins#reject', as: :reject_club_request

  devise_for :students

  resources :bookings

  resources :tickets

  resources :events
  post 'events/add_ticket' => 'events#add_ticket', as: :add_ticket
  post 'events/book_ticket' => 'events#book_ticket', as: :book_ticket
  post 'events/:id/invite' => 'events#invite', as: :invite
  post 'events/:id/submit_invite' => 'events#submit_invite', as: :submit_invite
  post 'events/:id/comment' => 'events#comment', as: :comment

  resources :clubs
  post 'clubs/subscribe' => 'clubs#subscribe', as: :subscribe
  post 'clubs/unsubscribe' => 'clubs#unsubscribe', as: :unsubscribe
  post 'clubs/create_event' => 'clubs#create_event', as: :create_event
  post 'clubs/:id/announce' => 'clubs#announce', as: :announce
  post 'clubs/:id/accept_invite' => 'clubs#accept_invite', as: :accept_invite
  post 'clubs/:id/reject_invite' => 'clubs#reject_invite', as: :reject_invite

  get "visitors/index", to:'visitors#index'
  get "students/index"
  #get ":id", to:"clubs#show"
  
  root "visitors#index"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
