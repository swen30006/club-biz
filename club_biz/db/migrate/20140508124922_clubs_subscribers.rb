class ClubsSubscribers < ActiveRecord::Migration
  def change
  	create_table :clubs_subscribers, id: false do |t|
      #t.belongs_to :subscribed_club, :class_name => "Club"
      #t.belongs_to :subscriber, :class_name => "Student"
      t.belongs_to :club
      t.belongs_to :student
    end
  end
end
