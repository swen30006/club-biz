class CreateClubs < ActiveRecord::Migration
  def change
    create_table :clubs do |t|
      t.string :name
      t.text :description
      t.text :contact
      t.integer :reg_num
      t.belongs_to :admin, :class_name => "Student"
      
      t.timestamps
    end
  end
end
