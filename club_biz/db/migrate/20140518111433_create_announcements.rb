class CreateAnnouncements < ActiveRecord::Migration
  def change
    create_table :announcements do |t|
      t.text :content

      t.belongs_to :club

      t.timestamps
    end
  end
end
