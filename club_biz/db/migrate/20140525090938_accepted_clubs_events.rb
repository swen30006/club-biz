class AcceptedClubsEvents < ActiveRecord::Migration
  def change
  	create_table :accepted_clubs_events, id: false do |t|
      #t.belongs_to :member_club, :class_name => "Club"
      #t.belongs_to :member, :class_name => "Student"
      t.belongs_to :club
      t.belongs_to :event
    end
  end
end
