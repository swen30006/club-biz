class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :name
      t.integer :price
      t.date :start_date
      t.date :close_date
      t.text :msg
      t.integer :max_no

      t.belongs_to :event
      
      t.timestamps
    end
  end
end
