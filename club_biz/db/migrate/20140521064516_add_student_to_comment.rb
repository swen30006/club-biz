class AddStudentToComment < ActiveRecord::Migration
  def change
  	add_reference :comments, :student, index: true
  end
end
