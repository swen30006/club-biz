class CreateClubRequests < ActiveRecord::Migration
  def change
    create_table :club_requests do |t|
      t.string :name
      t.integer :regis_num
      t.string :admin
      t.text :contact

      t.timestamps
    end
  end
end
