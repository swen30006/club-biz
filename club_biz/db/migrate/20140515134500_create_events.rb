class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.date :date
      t.time :time
      t.text :description
      t.text :location

      t.belongs_to :org_club, :class_name => "Club"
      
      t.timestamps
    end
  end
end
