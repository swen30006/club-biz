class ClubsMembers < ActiveRecord::Migration
  def change
  	create_table :clubs_members, id: false do |t|
      #t.belongs_to :member_club, :class_name => "Club"
      #t.belongs_to :member, :class_name => "Student"
      t.belongs_to :club
      t.belongs_to :student
    end
  end
end
