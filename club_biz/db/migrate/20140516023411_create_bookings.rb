class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :num_tickets
      t.belongs_to :ticket
      t.belongs_to :student

      t.timestamps
    end
  end
end
