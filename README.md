Club-Biz is a Web App developed by a group of unimelb students for the subject Software Modelling and Design(SWEN30006)

Club-Biz is a useful, usable, interactive app that allows clubs and societies of a particular university to grow their membership and provide excellent support for events as well as to increase participation in clubs and in club events.

Note : Application was developed and tested using Ruby 2.1.0 and Rails 4.1.4